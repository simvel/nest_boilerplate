import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SuperloggerModule } from './superlogger/superlogger.module';
import { SuperloggerService } from './superlogger/services/superlogger/superlogger.service';
import { OPTIONS } from './superlogger/interfaces/options.interface';

describe('AppController', () => {
	let appController: AppController;

	beforeEach(async () => {
		const app: TestingModule = await Test.createTestingModule({
			controllers: [AppController],
			providers: [
				AppService,
				SuperloggerService,
				{
					provide: OPTIONS,
					useValue: { prefix: 'test' },
				},
			],
			imports: [SuperloggerModule.forRoot({ prefix: 'test' })],
		}).compile();

		appController = app.get<AppController>(AppController);
	});

	describe('root', () => {
		it('should return "Hello World!"', () => {
			expect(appController.getHello()).toBe('Hello World!');
		});
	});
});
