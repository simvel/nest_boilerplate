import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SuperloggerModule } from './superlogger/superlogger.module';
import { UserModule } from './user/user.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { appConfig } from './config/app.config';
import { databaseConfig } from './config/db.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user/user-entity/user.entity';
import { Book } from './book/book-entity/book-entity';
import { BookModule } from './book/book.module';

@Module({
	controllers: [AppController],
	providers: [AppService],
	imports: [
		UserModule,
		BookModule,
		ConfigModule.forRoot({
			load: [appConfig, databaseConfig],
			isGlobal: true,
		}),
		TypeOrmModule.forRootAsync({
			useFactory: async (configService: ConfigService) => {
				if (process.env.APPLICATION_ENV === 'test') {
					return {
						type: 'better-sqlite3',
						database: ':memory:',
						entities: [User, Book],
						synchronize: true,
						dropSchema: true,
					}
				}
				return {
					...configService.get('database'),
					entities: [User, Book],
					synchronize: true
				}
			},
			inject: [ConfigService],
		}),
	],
})
export class AppModule {}
