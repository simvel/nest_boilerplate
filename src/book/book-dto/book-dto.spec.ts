import { validate } from 'class-validator';
import { BookDto } from './book-dto';

describe('BookDto', () => {
	it('should be defined', () => {
		expect(new BookDto()).toBeDefined();
	});

	it('should pass validation with a valid title and author', async () => {
		const bookDto = new BookDto();
		bookDto.title = 'Valid Title';
		bookDto.author = 'Valid Author';
		const errors = await validate(bookDto);
		expect(errors.length).toBe(0);
	});

	it('should fail validation without a title', async () => {
		const bookDto = new BookDto();
		bookDto.author = 'Valid Author';
		const errors = await validate(bookDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isNotEmpty');
	});

	it('should fail validation with a non-string title', async () => {
		const bookDto = new BookDto();
		bookDto.title = 123 as any;
		bookDto.author = 'Valid Author';
		const errors = await validate(bookDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isString');
	});

	it('should fail validation without author', async () => {
		const bookDto = new BookDto();
		bookDto.title = 'Valid Title';
		const errors = await validate(bookDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isNotEmpty');
	})

	it('should fail validation with a non-string author', async () => {
		const bookDto = new BookDto();
		bookDto.title = 'Valid Title';		
		bookDto.author = 123 as any;
		const errors = await validate(bookDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isString');
	})
});
