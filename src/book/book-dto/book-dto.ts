import { IsNotEmpty, IsString, MaxLength } from "class-validator";

export class BookDto {
	@IsString()
	@IsNotEmpty()
	@MaxLength(100)
	title: string;

	@IsString()
	@IsNotEmpty()
	@MaxLength(100)
	author: string;
}
