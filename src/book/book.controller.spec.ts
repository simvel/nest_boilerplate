import { Test, TestingModule } from '@nestjs/testing';
import { BookController } from './book.controller';
import { BookService } from './book.service';

describe('BookController', () => {
	let controller: BookController;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				{
					provide: BookService,
					useValue: {}
				}
			],
			controllers: [BookController],
		}).compile();

		controller = module.get<BookController>(BookController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});

	it('should have a getAll method', () => {
	  expect(controller).toHaveProperty('getAll');
	});
  
	it('should have a getByIndex method', () => {
	  expect(controller).toHaveProperty('getByIndex')
	});
  
	it('should have a create method', () => {
	  expect(controller).toHaveProperty('create')
	});
  
	it('should have an update method', () => {
	  expect(controller).toHaveProperty('update')
	});
  
	it('should have a delete method', () => {
	  expect(controller).toHaveProperty('delete')
	});
});
