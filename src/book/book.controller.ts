import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	NotFoundException,
	Param,
	Post,
	Put,
} from '@nestjs/common';
import { BookDto } from './book-dto/book-dto';
import { BookService } from './book.service';
import { Book } from './book-entity/book-entity';

@Controller('book')
export class BookController {
	constructor(private bookService: BookService) {}

	@Get()
	async getAll(): Promise<Book[]> {
		return await this.bookService.list();
	}

	@Get('/:index')
	async getByIndex(@Param('index') index: number): Promise<Book>  {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a valid value.`)
		}
		const book = await this.bookService.get(index)
		if (!book) {
			throw new NotFoundException(`Book with index ${index} does not exist.`)
		}
		return book;
	}

	@Post()
	create(@Body() bookDto: BookDto): Promise<Book> {
		return this.bookService.create(bookDto);
	}

	@Put('/:index')
	async update(@Param('index') index: number, @Body() bookDto: BookDto): Promise<Book> {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a number.`)
		}
		const book = await this.bookService.get(index)
		if (!book) {
			throw new NotFoundException(`Book with index ${index} does not exist.`)
		}
		return this.bookService.update(index, bookDto);
	}

	@Delete('/:index')
	async delete(@Param('index') index: number): Promise<void> {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a number.`)
		}
		const book = await this.bookService.get(index)
		if (!book) {
			throw new NotFoundException(`Book with index ${index} does not exist.`)
		}
		this.bookService.deleteBook(index);
	}
}
