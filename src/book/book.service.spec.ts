import { Test, TestingModule } from '@nestjs/testing';
import { BookService } from './book.service';
import { NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Book } from './book-entity/book-entity';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';

describe('BookService', () => {
  let module: TestingModule;
  let service: BookService;
  let bookRepository: Repository<Book>;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'better-sqlite3',
          database: ':memory:',
          entities: [Book],
          synchronize: true,
          dropSchema: true,
        }),
        TypeOrmModule.forFeature([Book]),
      ],
      providers: [BookService],
    }).compile();

    service = module.get<BookService>(BookService);
    bookRepository = module.get<Repository<Book>>(getRepositoryToken(Book));

    await bookRepository.insert([
      bookRepository.create({
        title: 'La vie est une jungle',
        author: 'Tar Zan'
      })
    ]);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return books', async () => {
    const books: Book[] = await service.list();
    expect(books).toBeInstanceOf(Array);
    expect(books[0]).toBeInstanceOf(Book);
    expect(books[0].title).toBe('La vie est une jungle');
    expect(books[0].author).toBe('Tar Zan')
  })

  it('should return book by index', async () => {
    const book: Book = await service.get(1);
    expect(book).toBeInstanceOf(Book);
    expect(book.title).toBe('La vie est une jungle');
    expect(book.author).toBe('Tar Zan')
  })

  it('should throw ReferenceError if index does not exist', () => {
     try {
      service.get(2);
      fail('Expected method to throw an error');
     } catch (error) {
      expect(error).toBeInstanceOf(ReferenceError);
     }
  })

  it('should add new book', async () => {
    const initialAllBooks = await service.list();
    await service.create({title: 'test', author: 'test'});
    const modifyAllBooks = await service.list();
    expect(modifyAllBooks.length).toBeGreaterThan(initialAllBooks.length);
    expect(modifyAllBooks[1].title).toBe('test');
    expect(modifyAllBooks[1].author).toBe('test');
  })

  it('should edit book', async () => {
    const book: Book = await service.get(1);
    await service.update(1, {title: 'test', author: 'test'});
    const bookUpdated: Book = await service.get(1);
    expect(bookUpdated).not.toBe(book);
    expect(bookUpdated.title).toBe('test');
    expect(bookUpdated.author).toBe('test');
  })

  it('should throw ReferenceError if index does not exist', () => {
    try {
      service.update(2, {title: 'test', author: 'test'});
      fail('Expected method to throw an error');
    } catch (error) {
      expect(error).toBeInstanceOf(ReferenceError);
    }
  })

  it('should delete book', async () => {
    let initialAllBooks: Book[] = await service.list();
    await service.deleteBook(1);
    let modifyAllBooks: Book[] = await service.list();
    expect(modifyAllBooks.length).toBeLessThan(initialAllBooks.length);
  })

  it('should throw ReferenceError if index does not exist', () => {
    try {
      service.deleteBook(1);
      fail('Expected method to throw an error');
    } catch (error) {
      expect(error).toBeInstanceOf(ReferenceError);
    }
  })
});
