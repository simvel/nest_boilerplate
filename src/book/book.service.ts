import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from './book-entity/book-entity';
import { DeleteResult, Repository } from 'typeorm';
import { BookDto } from './book-dto/book-dto';

@Injectable()
export class BookService {
	constructor(@InjectRepository(Book) private bookRepository: Repository<Book>) {}

	list(): Promise<Book[]> {
		return this.bookRepository.find();
	}

	get(id: number): Promise<Book> {
		return this.bookRepository.findOne({where: {id: id}});
	}

	async create(bookDto: BookDto): Promise<Book> {
		const book = this.bookRepository.create(bookDto);
		await this.bookRepository.save(book);
		return book;
	}

	async update(id: number, bookDto: BookDto): Promise<Book> {
		await this.bookRepository.update(id, bookDto);
		return this.get(id);
	}

	deleteBook(id: number): Promise<DeleteResult> {
		return this.bookRepository.delete(id);
	}
}
