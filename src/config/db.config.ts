import { registerAs } from "@nestjs/config";

export interface DatabaseConfig {
	type: string;
	host: string;
	port: number;
	database: string;
	username: string;
	password: string;
}

export const databaseConfig = registerAs('database', () => ({
	type: process.env.DB_TYPE,
	host: process.env.DB_HOST || 'localhost',
	port: parseInt(process.env.DB_PORT) || 5432,
	database: process.env.DB_NAME,
	username: process.env.DB_USERNAME || 'root',
	password: process.env.DB_PASSWORD || 'root',
}))