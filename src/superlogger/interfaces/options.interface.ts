export const OPTIONS = "options"

export interface SuperloggerOptions {
	prefix: string;
}