import { Test, TestingModule } from '@nestjs/testing';
import { SuperloggerService } from './superlogger.service';
import { SuperloggerModule } from 'src/superlogger/superlogger.module';

describe('SuperloggerService', () => {
  let service: SuperloggerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [SuperloggerModule.forRoot({ prefix: 'test' })],
    }).compile();

    service = module.get<SuperloggerService>(SuperloggerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should log message with prefix', () => {
    const consoleSpy = jest.spyOn(console, 'log');
    service.log();
    expect(consoleSpy).toHaveBeenCalledWith(expect.stringMatching(/^test/));
  })
});
