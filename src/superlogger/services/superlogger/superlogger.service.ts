import { Inject, Injectable } from '@nestjs/common';
import { OPTIONS, SuperloggerOptions } from '../../interfaces/options.interface';

@Injectable()
export class SuperloggerService {
	constructor(@Inject(OPTIONS) private readonly options: SuperloggerOptions) {}

	log(): void {
		console.log(`${this.options.prefix}_${new Date().toDateString()}`)
	}
}
