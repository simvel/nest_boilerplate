import { DynamicModule, Module } from '@nestjs/common';
import { SuperloggerService } from './services/superlogger/superlogger.service';
import { OPTIONS, SuperloggerOptions } from './interfaces/options.interface';

@Module({})
export class SuperloggerModule {
	static forRoot(options: SuperloggerOptions): DynamicModule {
		return {
			module: SuperloggerModule,
			providers: [
				{
					provide: OPTIONS,
					useValue: options,
				},
				SuperloggerService,
			],
			exports: [SuperloggerService],
		}
	}
}
