import { validate } from 'class-validator';
import { UserDto } from './user-dto';

describe('UserDto', () => {
	it('should be defined', () => {
		expect(new UserDto()).toBeDefined();
	});

	it('should pass validation with a valid name', async () => {
		const userDto = new UserDto();
		userDto.firstname = 'Valid'
		userDto.lastname = 'Name';
		const errors = await validate(userDto);
		expect(errors.length).toBe(0);
	});

	it('should fail validation without a name', async () => {
		const userDto = new UserDto();
		const errors = await validate(userDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isNotEmpty');
	});

	it('should fail validation with a non-string name', async () => {
		const userDto = new UserDto();
		userDto.firstname = 123 as any;
		userDto.lastname = 123 as any;
		const errors = await validate(userDto);
		expect(errors.length).toBeGreaterThan(0);
		expect(errors[0].constraints).toHaveProperty('isString');
	});
});
