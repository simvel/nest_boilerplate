import { IsNotEmpty, IsString, MaxLength } from "class-validator";

export class UserDto {
	@IsString()
	@IsNotEmpty()
	@MaxLength(30)
	firstname: string;

	@IsString()
	@IsNotEmpty()
	@MaxLength(30)
	lastname: string;
}
