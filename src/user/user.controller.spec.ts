import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from './user-entity/user.entity';

describe('UserController', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: UserService,
          useValue: {}
        }
      ],
      controllers: [UserController],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should have a getAll method', () => {
    expect(controller).toHaveProperty('getAll');
  });

  it('should have a getByIndex method', () => {
    expect(controller).toHaveProperty('getByIndex')
  });

  it('should have a create method', () => {
    expect(controller).toHaveProperty('create')
  });

  it('should have an update method', () => {
    expect(controller).toHaveProperty('update')
  });

  it('should have a delete method', () => {
    expect(controller).toHaveProperty('delete')
  });
});
