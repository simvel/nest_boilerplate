import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	NotFoundException,
	Param,
	Post,
	Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './user-dto/user-dto';
import { User } from './user-entity/user.entity';

@Controller('user')
export class UserController {
	constructor(private userService: UserService) {}

	@Get()
	async getAll(): Promise<User[]> {
		return await this.userService.list();
	}

	@Get('/:index')
	async getByIndex(@Param('index') index: number): Promise<User> {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a valid value.`);
		}
		const user = await this.userService.get(index);
		if (!user) {
			throw new NotFoundException(
				`User with index ${index} does not exist.`,
			);
		}
		return user;
	}

	@Post()
	create(@Body() userDto: UserDto): Promise<User> {
		return this.userService.create(userDto);
	}

	@Put('/:index')
	async update(
		@Param('index') index: number,
		@Body() userDto: UserDto,
	): Promise<User> {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a valid value.`);
		}
		const user = await this.userService.get(index);
		if (!user) {
			throw new NotFoundException(
				`User with index ${index} does not exist.`,
			);
		}
		return this.userService.update(index, userDto);
	}

	@Delete('/:index')
	async delete(@Param('index') index: number): Promise<void> {
		if (isNaN(index)) {
			throw new BadRequestException(`${index} is not a valid value.`);
		}
		const user = await this.userService.get(index);
		if (!user) {
			throw new NotFoundException(
				`User with index ${index} does not exist.`,
			);
		}
		this.userService.delete(index);
	}
}
