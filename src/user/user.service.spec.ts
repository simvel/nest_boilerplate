import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { NotFoundException } from '@nestjs/common';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { User } from './user-entity/user.entity';
import { Repository } from 'typeorm';

describe('UserService', () => {
	let module: TestingModule;
	let service: UserService;
	let userRepository: Repository<User>

	beforeEach(async () => {
		module = await Test.createTestingModule({
			imports: [
				TypeOrmModule.forRoot({
					type: 'better-sqlite3',
					database: ':memory:',
					entities: [User],
					synchronize: true,
					dropSchema: true,
				}),
				TypeOrmModule.forFeature([User]),
			],
			providers: [UserService],
		}).compile();

		service = module.get<UserService>(UserService);
		userRepository = module.get<Repository<User>>(getRepositoryToken(User));

		await userRepository.insert([
			userRepository.create({
				firstname: 'Tar',
				lastname: 'Zan'
			})
		])
	});

	afterEach(async () => {
		await module.close();
	})

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	it('should return users', async () => {
		const users: User[] = await service.list();
		expect(users).toBeInstanceOf(Array);
		expect(users[0]).toBeInstanceOf(User);
	});

	it('should return user by index', async () => {
		const user: User = await service.get(1)
		expect(user).toBeInstanceOf(User);
		expect(typeof user.firstname).toBe("string")
		expect(typeof user.lastname).toBe("string")

	});

	it('should throw ReferenceError if index does not exist', () => {
		try {
			service.get(0);
			fail('Expected method to throw an error');
		} catch (error) {
			expect(error).toBeInstanceOf(ReferenceError);
		}
	});

	it('should add new user', async () => {
		let initialAllUsers: User[] = await service.list();
		await service.create({firstname: 'Josette', lastname: 'Delgado'});
		let modifyAllUsers: User[] = await service.list();
		expect(modifyAllUsers.length).toBeGreaterThan(initialAllUsers.length);
		expect(modifyAllUsers[1].firstname).toBe('Josette');
		expect(modifyAllUsers[1].lastname).toBe('Delgado');
	});

	it('should edit user by index', async () => {
		const user: User = await service.get(1);
		await service.update(1, {firstname: 'Bob', lastname: 'Marley'});
		const userUpdated: User = await service.get(1);
		expect(userUpdated).not.toBe(user);
		expect(userUpdated.firstname).toBe('Bob');
		expect(userUpdated.lastname).toBe('Marley')
	});

	it('should throw ReferenceError if index does not exist', () => {
		try {
			service.update(1, {firstname: 'Bob', lastname: 'Marley'});
			fail('Expected method to throw an error');
		} catch (error) {
			expect(error).toBeInstanceOf(ReferenceError);
		}
	});

	it('should delete user by index', async () => {
		let initialAllUsers: User[] = await service.list();
		await service.delete(1);
		let modifyAllUsers: User[] = await service.list();
		expect(modifyAllUsers.length).toBeLessThan(initialAllUsers.length);
	});

	it('should throw ReferenceError if index does not exist', () => {
		try {
			service.delete(1);
			fail('Expected method to throw an error');
		} catch (error) {
			expect(error).toBeInstanceOf(ReferenceError);
		}
	});
});
