import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user-entity/user.entity';
import { DeleteResult, Repository } from 'typeorm';
import { UserDto } from './user-dto/user-dto';

@Injectable()
export class UserService {
	constructor(@InjectRepository(User) private userRepository: Repository<User>) {}

	list(): Promise<User[]> {
	  return this.userRepository.find();
	}

	get(id: number): Promise<User> {
		return this.userRepository.findOne({where: {id: id}});
	}

	async create(userDto: UserDto): Promise<User> {
		const user = this.userRepository.create(userDto);
		await this.userRepository.save(user);
		return user;
	}

	async update(id: number, userDto: UserDto): Promise<User> {
		await this.userRepository.update(id, userDto);
		return this.get(id);
	}

	delete(id: number): Promise<DeleteResult> {
		return this.userRepository.delete(id);
	}
}
