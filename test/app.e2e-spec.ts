import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { User } from '../src/user/user-entity/user.entity';
import { Book } from '../src/book/book-entity/book-entity';

describe('e2e', () => {
	let app: INestApplication;
	let moduleFixture: TestingModule;
	process.env.APPLICATION_ENV = 'test';

	beforeEach(async () => {
		moduleFixture = await Test.createTestingModule({
			imports: [AppModule],
		}).compile();

		app = moduleFixture.createNestApplication();
		app.useGlobalPipes(
			new ValidationPipe({
				whitelist: true,
				forbidNonWhitelisted: true,
			}),
		);
		await app.init();
	});

	describe('AppController', () => {
		it('/ (GET)', () => {
			return request(app.getHttpServer())
				.get('/')
				.expect(200)
				.expect('Hello World!');
		});
	});

	describe('BookController', () => {
		let bookRepository: Repository<Book>;
		beforeEach(async () => {
			bookRepository = moduleFixture.get<Repository<Book>>(
				getRepositoryToken(Book),
			);

			await bookRepository.insert([
				bookRepository.create({
					title: 'La vie est une jungle',
					author: 'Tar Zan',
				}),
			]);
		});
		
		it('/book (GET)(200)', () => {
			return request(app.getHttpServer())
				.get('/book')
				.expect(200)
				.expect('Content-Type', /json/)
				.then((res) => {
					expect(res.body).toBeInstanceOf(Array);
				});
		});

		it('/book/:id (GET)(200)', () => {
			return request(app.getHttpServer())
				.get('/book/1')
				.expect(200)
				.then((res) => {
					expect(typeof res.text).toMatch('string');
				});
		});

		it('/book/:id inexistant (GET)(404)', () => {
			return request(app.getHttpServer()).get('/book/2').expect(404);
		});

		it('/book/:id NaN (GET)(400)', () => {
			return request(app.getHttpServer()).get('/book/a').expect(400);
		});

		it('/book (POST)(201)', async () => {
			await request(app.getHttpServer())
				.post('/book')
				.send({
					title: 'Super book',
					author: 'Me'
				})
				.expect(201);

			await request(app.getHttpServer())
				.get('/book/2')
				.expect(200)
				.then((res) => {
					expect(res.body.title).toBe('Super book');
					expect(res.body.author).toBe('Me');
				})
		});

		it('/book/:id (PUT)(200)', async () => {
			await request(app.getHttpServer())
				.put('/book/1')
				.send({
					title: 'nouveau titre',
					author: 'me'
				})
				.expect(200);

			await request(app.getHttpServer())
				.get('/book/1')
				.expect(200)
				.then((res) => {
					expect(res.body.title).toBe('nouveau titre');
					expect(res.body.author).toBe('me')
				});
		});

		it('/book/:id inexistant (PUT)(404)', async () => {
			await request(app.getHttpServer())
				.put('/book/2')
				.send({
					title: 'nouveau titre',
					author: 'me',
				})
				.expect(404);
		});

		it('/book/:id NaN (PUT)(400)', async () => {
			await request(app.getHttpServer())
				.put('/book/a')
				.send({
					title: 'nouveau titre',
					author: 'me',
				})
				.expect(400);
		});

		it('/book/:id (DELETE)(200)', async () => {
			await request(app.getHttpServer()).delete('/book/1').expect(200);

			await request(app.getHttpServer()).get('/book/1').expect(404);
		});

		it('/book/:id inexistant (DELETE)(404)', () => {
			return request(app.getHttpServer()).delete('/book/2').expect(404);
		});

		it('/book/:id NaN (DELETE)(400)', () => {
			return request(app.getHttpServer()).delete('/book/a').expect(400);
		});
	});

	describe('UserController', () => {
		let userRepository: Repository<User>;
		beforeEach(async () => {
			userRepository = moduleFixture.get<Repository<User>>(
				getRepositoryToken(User),
			);

			await userRepository.insert([
				userRepository.create({
					firstname: 'Tar',
					lastname: 'Zan',
				}),
			]);
		});

		it('/user (GET)(200)', () => {
			return request(app.getHttpServer())
				.get('/user')
				.expect(200)
				.expect('Content-Type', /json/)
				.then((res) => {
					expect(res.body).toBeInstanceOf(Array);
				});
		});
	
		it('/user/:id (GET)(200)', () => {
			return request(app.getHttpServer())
				.get('/user/1')
				.expect(200)
				.then((res) => {
					expect(typeof res.text).toMatch('string');
				});
		});
	
		it('/user/:id inexistant (GET)(404)', () => {
			return request(app.getHttpServer()).get('/user/2').expect(404);
		});

		it('/user/:id NaN (GET)(400)', () => {
			return request(app.getHttpServer()).get('/user/a').expect(400);
		})
	
		it('/user (POST)(201)', async () => {
			const users: string[] = await request(app.getHttpServer())
				.get('/user')
				.then((res) => {
					return res.body;
				});
			await request(app.getHttpServer())
				.post('/user')
				.send({
					firstname: 'Raoul',
					lastname: 'Delgado',
				})
				.expect(201);
			await request(app.getHttpServer())
				.get('/user')
				.then((res) => {
					expect(res.body.length).toBeGreaterThan(users.length);
				});
		});
	
		it('/user/:id (PUT)(201)', () => {
			return request(app.getHttpServer())
				.put('/user/1')
				.send({ 
					firstname: 'Raoul',
					lastname: 'Delgado',
				})
				.expect(200)
				.then((res) => {
					expect(res.body.firstname).toBe('Raoul');
					expect(res.body.lastname).toBe('Delgado');
				});
		});
	
		it('/user/:id inexistant (put)(404)', () => {
			return request(app.getHttpServer())
				.put('/user/2')
				.send({ 
					firstname: 'Raoul',
					lastname: 'Delgado',
				})
				.expect(404);
		});

		it('/user/:id NaN (put)(400)', () => {
			return request(app.getHttpServer())
				.put('/user/a')
				.send({ 
					firstname: 'Raoul',
					lastname: 'Delgado',
				})
				.expect(400);
		});
	
		it('/user/:id (DELETE)(200)', async () => {
			await request(app.getHttpServer()).delete('/user/1').expect(200);
	
			await request(app.getHttpServer()).get('/user/1').expect(404);
		});
	
		it('/user/:id inexistant (DELETE)(404)', () => {
			return request(app.getHttpServer()).delete('/user/0').expect(404);
		});

		it('/user/:id NaN (DELETE)(400)', () => {
			return request(app.getHttpServer()).delete('/user/a').expect(400);
		})
	})
});
